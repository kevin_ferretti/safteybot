﻿namespace safetybot.Domain.Configuration
{
    public class SafetyBotSecrets
    {
        public string Token { get; set; }
    }
}
