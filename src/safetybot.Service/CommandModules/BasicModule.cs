﻿using System;
using System.Threading.Tasks;
using Discord.Commands;
using Discord.WebSocket;

namespace safetybot.Service.CommandModules
{
    [Name("Basic")]
    [Summary("Basic commands with simple responses")]
    public class BasicModule : ModuleBase<SocketCommandContext>
    {
        private readonly Random _random;

        public BasicModule()
        {
            _random = new Random();
        }

        /// <summary>
        /// .say hi -> hi
        /// </summary>
        [Command("say"), Alias("s")]
        [Summary("Make the bot say something")]
        public async Task Say([Remainder] [Summary("The text to say")] string text) => 
            await ReplyAsync(text);

        /// <summary>
        /// .camden -> jim
        /// </summary>
        [Command("camden")]
        [Summary("Camden_Jim")]
        public async Task CamdenAsync() =>
            await ReplyAsync("jim");

        /// <summary>
        /// .pickone coice1 choice2 choice3 -> (random choice)
        /// </summary>
        [Command("pickone"), Alias("p1")]
        [Summary("Randomly picks one of the supplied space separated options")]
        public async Task PickOne(params string[] options)
        {
            var randomIndex = _random.Next(0, options.Length);
            await ReplyAsync(options[randomIndex]);
        }

        /// <summary>
        /// .8ball -> (random 8ball response)
        /// </summary>
        [Command("8ball"), Alias("8b", "eightball")]
        [Summary("Magic 8 ball")]
        public Task EightBall(params string[] placeholderParams)
        {
            // todo: put these in separate file
            switch (_random.Next(1, 20))
            {
                case 1:
                    return ReplyAsync("As I see it, yes.");
                case 2:
                    return ReplyAsync("Ask again later.");
                case 3:
                    return ReplyAsync("Better not tell you now.");
                case 4:
                    return ReplyAsync("Cannot predict now.");
                case 5:
                    return ReplyAsync("Concentrate and ask again.");
                case 6:
                    return ReplyAsync("Don’t count on it.");
                case 7:
                    return ReplyAsync("It is certain.");
                case 8:
                    return ReplyAsync("It is decidedly so.");
                case 9:
                    return ReplyAsync("Most likely.");
                case 10:
                    return ReplyAsync("My reply is no.");
                case 11:
                    return ReplyAsync("My sources say no.");
                case 12:
                    return ReplyAsync("Outlook not so good.");
                case 13:
                    return ReplyAsync("Outlook good.");
                case 14:
                    return ReplyAsync("Reply hazy, try again.");
                case 15:
                    return ReplyAsync("Signs point to yes.");
                case 16:
                    return ReplyAsync("Very doubtful.");
                case 17:
                    return ReplyAsync("Without a doubt.");
                case 18:
                    return ReplyAsync("Yes.");
                case 19:
                    return ReplyAsync("Yes – definitely.");
                case 20:
                    return ReplyAsync("You may rely on it.");
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// <list type="bullet">
        ///     <item>
        ///         .userinfo --> kfred#1502
        ///     </item>
        ///     <item>
        ///         .userinfo @kfred --> kfred#1502
        ///     </item>
        ///     <item>
        ///         .userinfo kfred#1502 --> kfred#1502
        ///     </item>
        ///     <item>
        ///         .userinfo kfred --> kfred#1502
        ///     </item>
        ///     <item>
        ///         .userinfo 268972099522002944 --> kfred#1502
        ///     </item>
        ///     <item>
        ///         .userinfo 268972099522002944 --> kfred#1502
        ///     </item>
        /// </list>
        /// </summary>
        [Command("userinfo")]
        [Summary("Returns info about the current user, or the user parameter, if one is passed.")]
        [Alias("user", "whois")]
        public async Task UserInfoAsync([Summary("The optional user to return info for")] SocketUser user = null)
        {
            var userInfo = user ?? Context.User;
            await ReplyAsync($"{userInfo.Username}#{userInfo.Discriminator} {userInfo.GetAvatarUrl()}");
        }
    }
}
