﻿using safetybot.Domain.Configuration;

namespace safetybot.Service
{
    public interface ISecretRevealer
    {
        SafetyBotSecrets Reveal();
    }
}