﻿using System;
using Microsoft.Extensions.Options;
using safetybot.Domain.Configuration;

namespace safetybot.Service
{
    public class SecretRevealer : ISecretRevealer
    {
        private readonly SafetyBotSecrets _secrets;

        public SecretRevealer(IOptions<SafetyBotSecrets> secrets)
        {
            _secrets = secrets.Value ?? throw new ArgumentNullException(nameof(secrets));
        }

        public SafetyBotSecrets Reveal() => _secrets;
    }
}
