﻿using System.Threading.Tasks;

namespace safetybot.Core
{
    public class Program
    {
        private static Task Main()
            => Startup.RunAsync();
    }
}
