﻿using System;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using safetybot.Domain.Configuration;
using safetybot.Service;

namespace safetybot.Core
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }

        public Startup()
        {
            var builder = new ConfigurationBuilder();

            // todo: extract ////////////////////////////////////////////////
            var devEnvironmentVariable = Environment.GetEnvironmentVariable("NETCORE_ENVIRONMENT");
            var isDevelopment = string.IsNullOrEmpty(devEnvironmentVariable) ||
                                devEnvironmentVariable.ToLower() == "development";
            builder
                .AddJsonFile("appsettings.json", false, true);
            if (isDevelopment)
            {
                builder.AddUserSecrets<Program>();
            }
            //////////////////////////////////////////////////////////////////

            Configuration = builder.Build();
        }

        public static async Task RunAsync()
        {
            var startup = new Startup();
            await startup.Run();
        }

        public async Task Run()
        {
            var services = new ServiceCollection();
            ConfigureServices(services);

            var provider = services.BuildServiceProvider();
            provider.GetRequiredService<LoggingService>();
            provider.GetRequiredService<CommandHandler>();

            await provider.GetRequiredService<StartupService>().StartAsync();
            await Task.Delay(-1);
        }

        private void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(new DiscordSocketClient(new DiscordSocketConfig
                {
                    LogLevel = LogSeverity.Verbose,
                    MessageCacheSize = 1000
                }))
                .AddSingleton(new CommandService(new CommandServiceConfig
                {
                    LogLevel = LogSeverity.Verbose,
                    DefaultRunMode = RunMode.Async,
                    CaseSensitiveCommands = false
                }))
                .AddSingleton<CommandHandler>()
                .AddSingleton<StartupService>()
                .AddSingleton<LoggingService>()
                .Configure<SafetyBotSecrets>(Configuration.GetSection(nameof(SafetyBotSecrets)))
                .AddSingleton<ISecretRevealer, SecretRevealer>()
                .AddSingleton<Random>()
                .AddSingleton(Configuration);
        }
    }
}